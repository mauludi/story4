from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request, 'index.html')

def home(request):
	return render(request, 'home.html')

def about(request):
	response = {'':''}
	return render(request, 'about.html')

def gallery(request):
	response = {'':''}
	return render(request, 'gallery.html')

def contact(request):
	response = {'':''}
	return render(request, 'contact.html')