from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from status.views import index
from status.models import StatusPost
from status.forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class StatusUnitTest(TestCase):

	@classmethod
	def setUpTestData(cls):
		StatusPost.objects.create(status="Test")

	# test if url is exist
	def test_home_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	# test if url is not exist
	def test_url_is_not_exist(self):
		response = Client().get('/wek/')
		self.assertEqual(response.status_code, 404)

	# test if home using index func
	def test_home_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	# test if home using index.html
	def test_home_using_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	# test if home landing page is completed
	def test_home_landing_page_is_completed(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', html_response)

	# test if status is created and saved to database
	def test_if_status_is_in_database(self):
		status = StatusPost.objects.create(status="Test2")
		total_status = StatusPost.objects.all().count()
		self.assertEqual(total_status, 2)

	# test if status attribute is exist
	def test_if_status_status_is_exist(self):
		statusObj = StatusPost.objects.get(id=1)
		status = StatusPost._meta.get_field('status').verbose_name
		self.assertEquals(status, 'status')

	# test if status max length is 300
	def test_if_status_max_length_is_300(self):
		statusObj = StatusPost.objects.get(id=1)
		status = StatusPost._meta.get_field('status').max_length
		self.assertEquals(status, 300)

	# test if date attribute is exist
	def test_if_status_date_is_exist(self):
		statusObj = StatusPost.objects.get(id=1)
		date = StatusPost._meta.get_field('date').verbose_name
		self.assertEquals(date, 'date')

	# test if date auto_now_add is true
	def test_if_date_auto_is_true(self):
		statusObj = StatusPost.objects.get(id=1)
		date = StatusPost._meta.get_field('date').auto_now_add
		self.assertEquals(date, True)

	# test if status form has placeholder
	def test_status_form_has_placeholder(self):
		form = StatusForm()
		self.assertIn('id="id_status', form.as_p())

	# test status form blank validation
	def test_status_form_validation_for_blank_items(self):
		form = StatusForm(data={'status':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'], ["This field is required."])

	# test post success and render result
	def test_status_post_success_and_render_result(self):
		test = 'Test3'
		response_post = Client().post('/', {'status': test})
		self.assertEqual(response_post.status_code, 200)

		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	# test post error and render result
	def test_status_post_error_and_render_result(self):
		test = 'Test3'
		response_post = Client().post('/', {'status': ''})
		self.assertEqual(response_post.status_code, 200)

		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

class StatusFunctionalTest(LiveServerTestCase):

	# set up selenium
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)

    # close selenium
    def tearDown(self):
        self.selenium.quit()
        super(StatusFunctionalTest, self).tearDown()

    # main flow of functional test
    def test_input_todo(self):

    	# open selenium
        selenium = self.selenium

        # open localhost
        selenium.get('http://127.0.0.1:8000/')

        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_tag_name('button')

        # fill form with data
        status.send_keys('Coba Coba')

        # submit form
        submit.send_keys(Keys.RETURN)

        self.assertIn('Coba Coba', self.selenium.page_source)